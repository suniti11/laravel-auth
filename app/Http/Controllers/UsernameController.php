<?php
namespace App\Http\Controllers;

use Request;
use App\Http\Controllers\Controller;
use App\Username;
use Amranidev\Ajaxis\Ajaxis;
use URL;

/**
 * Class UsernameController
 *
 * @author  The scaffold-interface created at 2016-04-11 03:30:54pm
 * @link  https://github.com/amranidev/scaffold-interfac
 */
class UsernameController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function index()
    {
        $usernames = Username::all();
        return view('username.index',compact('usernames'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view('username.create'
                );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @return  \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = Request::except('_token');

        $username = new Username();

        
        $username->firstname = $input['firstname'];

        
        $username->lastname = $input['lastname'];

        
        $username->fullname = $input['fullname'];

        
        
        $username->save();

        return redirect('username');
    }

    /**
     * Display the specified resource.
     *
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(Request::ajax())
        {
            return URL::to('username/'.$id);
        }

        $username = Username::findOrfail($id);
        return view('username.show',compact('username'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Request::ajax())
        {
            return URL::to('username/'. $id . '/edit');
        }

        
        $username = Username::findOrfail($id);
        return view('username.edit',compact('username'
                )
                );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function update($id)
    {
        $input = Request::except('_token');

        $username = Username::findOrfail($id);
    	
        $username->firstname = $input['firstname'];
        
        $username->lastname = $input['lastname'];
        
        $username->fullname = $input['fullname'];
        
        
        $username->save();

        return redirect('username');
    }

    /**
     * Delete confirmation message by Ajaxis
     *
     * @link  https://github.com/amranidev/ajaxis
     *
     * @return  String
     */
    public function DeleteMsg($id)
    {
        $msg = Ajaxis::MtDeleting('Warning!!','Would you like to remove This?','/username/'. $id . '/delete/');

        if(Request::ajax())
        {
            return $msg;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function destroy($id)
    {
     	$username = Username::findOrfail($id);
     	$username->delete();
        return URL::to('username');
    }

}
