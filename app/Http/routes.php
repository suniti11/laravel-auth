<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::auth();

Route::get('/home', 'HomeController@index');

//username Resources
/********************* username ***********************************************/
Route::resource('username','UsernameController');
Route::post('username/{id}/update','UsernameController@update');
Route::get('username/{id}/delete','UsernameController@destroy');
Route::get('username/{id}/deleteMsg','UsernameController@DeleteMsg');
/********************* username ***********************************************/
