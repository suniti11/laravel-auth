<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UsernameController
 *
 * @author  The scaffold-interface created at 2016-04-11 03:30:54pm
 * @link  https://github.com/amranidev/scaffold-interfac
 */
class Username extends Model
{

    public $timestamps = false;

    protected $table = 'usernames';

	
}
