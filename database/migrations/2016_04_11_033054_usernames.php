<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class Usernames
 *
 * @author  The scaffold-interface created at 2016-04-11 03:30:54pm
 * @link  https://github.com/amranidev/scaffold-interfac
 */
class Usernames extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        Schema::create('usernames',function (Blueprint $table){

        $table->increments('id');
        
        $table->String('firstname');
        
        $table->String('lastname');
        
        $table->String('fullname');
        
        /**
         * Foreignkeys section
         */
        
        // type your addition here

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::drop('usernames');
     }
}
